<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Cars;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\JWTManager as JWT;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;


class UserController extends Controller
{
     public function register(Request $request){
        $validator = Validator::make($request->json()->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
                'password_confirmation' => 'required|min:6'
            ]);

            if($validator->fails()){
                return response()->json($validator->errors());
            }

            $user = User::create([
                'name' => $request->json()->get('name'),
                'email' => $request->json()->get('email'),
                'password' => Hash::make($request->json()->get('password')),
            ]);

            $token = JWTAuth::fromUser($user);

            return response()->json(compact('user','token'), 201);

    }

    public function login(Request $request){
        $credentials = $request->json()->all();

         $validator = Validator::make($credentials, [
                
                'email' => 'required|string|email|max:255',
                'password' => 'required|string|min:6',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors());
        }
        try {
            if(! $token = JWTAuth::attempt($credentials)){
                    return response()->json(['error'=>'invalid Credentials']);
            }
        }catch (JWTException $e){
            return response()->json(['error'=>'could_not_create_token']);
        }

        return response()->json(compact('token'));

    }

    public function getAuthenticatedUser(){
        try{
            if(!$user = JWTAuth::parseToken()->authenticate()){
                return response()->json(['user_not_found'], 400);
            }
        }catch (TokenExpiredException $e){
            return response()->json(['token_expired'], $e->getStatusCode());
        }catch (TokenInvalidException $e){
            return response()->json(['token_invalid'], $e->getStatusCode());
        }catch (JWTException $e){
            return response()->json(['token_absent'], $e->getStatusCode());
        }

        return response()->json(compact('user'));
    }

    public function car(Request $request){
       
         $cars = Cars::get();
        return response()->json(compact('cars'));
    }
    public function addCar(Request $request){

        if(!$request->car_id){
            return response()->json(['error'=>'Something went wrong.']);
        }
        $car = Cars::find($request->car_id);

        $car->in_stock = $car->in_stock +1;
        $car->save();
        $cars = Cars::get(); 
        return response()->json(compact('cars'));
    }
}