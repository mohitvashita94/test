<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Cars;


class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         

         $data = [
         	['car_name'=>'Tata Tiago'],
         	['car_name'=>'Tata Punch'],
         	['car_name'=>'Tata Nexon'],
			['car_name'=>'Tata Harrier'],
         	['car_name'=>'Tata Altroz'],
         	['car_name'=>'Tata Safari'],
         	['car_name'=>'Tata Tigor'],
         ];

         Cars::insert($data);
    }
}
