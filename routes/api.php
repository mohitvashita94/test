<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
 


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


// Route::namespace('Api')->group(function(){

	

	Route::post('register', [UserController::class, 'register']);
	Route::post('login', [UserController::class, 'login']);

	Route::group(['middleware'=>'jwt.verify'],function(){
		Route::get('get-user','UserController@getAuthenticatedUser');
		Route::get('get-cars', [UserController::class, 'car']);
		Route::post('add-car',[UserController::class, 'addCar']);
	});


// });