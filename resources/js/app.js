/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// import Vue from 'vue';
import { createApp } from 'vue'

import axios from 'axios';
import VueCookie from 'vue-cookie';
import VueAxios from 'vue-axios';
import VueRouter from 'vue-router';

import router from './router';

import App from './layouts/App';
import Header from './components/Header.vue';

// Vue.use(VueRouter);
// Vue.use(VueAxios,axios);




import Root from './components/ExampleComponent.vue';

const app = createApp(App);

app.use(router)
app.use(VueCookie)
app.provide('cookies',app.config.globalProperties.$cookies)

app.mount("#app");


// const app = new Vue({
// 	el:'#app',
// 	components: {App},
// 	router

// })
