// import Vue from 'vue';
// import Router from 'vue-router';
import { createWebHistory, createRouter } from "vue-router";

import Home from './page/Home.vue';
import Login from './page/Login.vue';
import Register from './page/Registration.vue';

// Vue.use(Router);

// const router = new Router({
// 	routes:[
// 		{path:'',name:'home',component:Home},
// 		{path:'/login',name:'login',component:Login},
// 		{path:'/register',name:'register',component:Register},

// 	]
// });

const routes = [
  	{path:'',name:'home',component:Home},
	{path:'/login',name:'login',component:Login},
	{path:'/register',name:'register',component:Register},
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});


export default router;